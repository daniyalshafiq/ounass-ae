package SiteTestCases;

import SiteElements.SiteActions;
import BaseClasses.DriverBaseClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class CheckOut extends DriverBaseClass {
    SiteActions siteActions;

    @BeforeClass(alwaysRun = true)
    public void initPageObjectClass(){ siteActions = new SiteActions(driver, wait);}

    @Parameters({"url"})
    @Test
    public void CheckOut(String url) throws InterruptedException{

        siteActions.navigateURL(url);
        siteActions.PageLoad();
        siteActions.CancelPopup();
        siteActions.CreateAccount();
        siteActions.GotoHomePage();
        siteActions.Search();
        siteActions.ClikCheckoutIcon();
        siteActions.CheckOut();
        siteActions.FillForm();
    }
}
