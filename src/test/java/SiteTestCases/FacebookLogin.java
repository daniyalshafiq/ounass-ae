package SiteTestCases;

import SiteElements.SiteActions;
import BaseClasses.DriverBaseClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class FacebookLogin extends DriverBaseClass {

    SiteActions siteActions;

    @BeforeClass (alwaysRun = true)
    public void intiPageObjectClass(){siteActions = new SiteActions(driver, wait);}

    @Parameters({"url"})
    @Test
    public void Login(String url) throws InterruptedException{
        siteActions.navigateURL(url);
        siteActions.PageLoad();
        siteActions.CancelPopup();
        siteActions.FBLogin();
    }

}
