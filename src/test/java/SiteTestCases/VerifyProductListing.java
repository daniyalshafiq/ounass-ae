package SiteTestCases;

import BaseClasses.DriverBaseClass;
import SiteElements.SiteActions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class VerifyProductListing extends DriverBaseClass {

    SiteActions siteActions;

    @BeforeClass(alwaysRun = true)
    public void initPageObjectClass() {
        siteActions = new SiteActions(driver, wait);
    }

    @Parameters({"url"})
    @Test
    public void VerifyProductsAndFilters(String url) throws InterruptedException {

        siteActions.navigateURL(url);
        siteActions.PageLoad();
        siteActions.CancelPopup();
        siteActions.NavigateToClothingPage();
        Thread.sleep(15000);
        siteActions.VerifyResults();
        siteActions.ApplyFilter();
        siteActions.VerifyFilterResults();
    }
}
