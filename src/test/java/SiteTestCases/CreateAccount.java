package SiteTestCases;

import SiteElements.SiteActions;
import BaseClasses.DriverBaseClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class CreateAccount extends DriverBaseClass {

    SiteActions siteActions;

    @BeforeClass(alwaysRun = true)
    public void initPageObectClass() {siteActions = new SiteActions(driver, wait);}

    @Parameters({"url"})
    @Test

    public void CreateAccount(String url) throws InterruptedException{
        siteActions.navigateURL(url);
        siteActions.CancelPopup();
        siteActions.CreateAccount();
        siteActions.VerifyEmailNonEditable();
    }

}
