package BaseClasses;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CreateRandomEmail{

    public static String generateTestEmail(){
        String Email;
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
        String TestName="John"+timeStamp+"@gmail.com";
        Email = TestName;
        return Email;
    }

}
