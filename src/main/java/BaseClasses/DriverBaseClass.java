package BaseClasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Parameters;

public class DriverBaseClass {

    protected WebDriver driver;
    protected WebDriverWait wait;


    public static WebDriver chrome_driver() {
        System.setProperty("webdriver.chrome.driver", "C:/Users/farhan.waseem/Desktop/chromedriver.exe");
        return null;
    }

    public static WebDriver firefox_driver(){
        System.setProperty("webdriver.firefox.driver", "Drivers/geckodriver.exe");
        return null;
    }

    @Parameters("url")
    @BeforeClass(alwaysRun = true)
    public void initDriver() throws InterruptedException {
        String url = "";
        driver = DriverBaseClass.chrome_driver();
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 15, 1000);
        driver.manage().window().maximize();
    }

    @AfterClass(alwaysRun = true)
   public void closeDriver() {
        driver.close();
    }

}
