package SiteElements;
import BaseClasses.CreateRandomEmail;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import Utilities.CommonActions;
import Utilities.GlobalVariables;

import java.util.List;

public class SiteActions extends SiteElements {

    private WebDriver driver;
    private WebDriverWait wait;
    private CommonActions commonActions;

    public SiteActions(WebDriver driver, WebDriverWait wait) {
        super();
        this.driver = driver;
        this.wait = wait;
        commonActions = new CommonActions(driver, wait);
        PageFactory.initElements(driver, this);
    }

    public void navigateURL(String url){
            commonActions.navigateToUrl(url);
    }
    public void PageLoad(){
        commonActions.WaitForURLLoad();
    }

    public void CancelPopup() throws InterruptedException {
        Thread.sleep(5000);
        if(PopUp.isDisplayed()){
            PopUpCancel.click();
        }
        else{
            System.out.println("No Popup");
        }
    }


    public void CreateAccount() throws InterruptedException {
        Thread.sleep(5000);
        AccountButton.click();
        Thread.sleep(3000);
        CreateAccountButton.click();
        ProfileFirstNameField.sendKeys(GlobalVariables.Firsname);
        ProfileLastNameField.sendKeys(GlobalVariables.LastName);
        ProfileEmailField.sendKeys(CreateRandomEmail.generateTestEmail());
        ProfilePasswordField.sendKeys(GlobalVariables.Password);
        ProfileCreateAccountButton.click();
        String EmailCreated = GlobalVariables.generateTestEmail();
        commonActions.waitForPageLoad();
    }

    public void FBLogin() throws InterruptedException {
            Thread.sleep(5000);
            AccountButton.click();
            FBButton.click();
            commonActions.waitForPageLoad();
            if (FBEmail.isDisplayed()){
                FBEmail.sendKeys(GlobalVariables.FBEmail);
                FBPassword.sendKeys(GlobalVariables.FBPass);
                FBLoginButton.click();
                commonActions.waitForPageLoad();
            }
            else{
                Thread.sleep(5000);
                FBEmail.sendKeys(GlobalVariables.FBEmail);
                FBPassword.sendKeys(GlobalVariables.FBPass);
                FBLoginButton.click();
                commonActions.waitForPageLoad();
            }
    }

    public void VerifyEmailNonEditable() throws InterruptedException {
        commonActions.waitForPageLoad();
        AccountLoggedinIcon.click();
        commonActions.waitForPageLoad();
        if(MyProfileLink.isDisplayed()) {
            MyProfileLink.click();
        }
        if(ProfileEmailField.getAttribute("value" ).contains("Not Editable")){
            System.out.println("Email Is Not editable");
        }
        if(ProfileEmailField.getAttribute("value").contains(CreateRandomEmail.generateTestEmail())){
            System.out.println("Same Email Is Displayed");
        }

    }
    public void GotoHomePage() throws InterruptedException {
        Logo.click();
        commonActions.waitForPageLoad();
    }

    public void Search() throws InterruptedException {
        SearchBar.click();
        Thread.sleep(2000);
        SearchBar.sendKeys(GlobalVariables.SearchTerm);
        Thread.sleep(6000);
        SelectSearchOption.click();
        Thread.sleep(2000);
        commonActions.waitForPageLoad();
        FirstProduct.click();
        commonActions.waitForPageLoad();
        AddToBagButton.click();
    }

    public void ClikCheckoutIcon() throws InterruptedException {
        Logo.click();
        commonActions.waitForPageLoad();
        BagIcon.click();
        BagIcon.click();
        commonActions.waitForPageLoad();
    }

    public void CheckOut() throws InterruptedException {
        SecureCheckOutButton.click();
        commonActions.waitForPageLoad();
    }

    public void FillForm(){
        PhoneNumber.click();
        PhoneNumber.sendKeys(GlobalVariables.PhoneNumber);
        AddressField.click();
        AddressField.sendKeys(GlobalVariables.Area);
        DeliveryField.click();
        DeliveryField.sendKeys(GlobalVariables.DeliveryAddress);
        ApartmentField.click();
        ApartmentField.sendKeys(GlobalVariables.AprtmentNumber);
        SubmittButton.click();

    }

    public void NavigateToClothingPage() throws InterruptedException {
        if (WomenTile.isDisplayed()){
            WomenTile.click();
            commonActions.waitForPageLoad();
        }
        else{
            System.out.println("Already On Women Page");
        }
        ClothingButton.click();
        commonActions.waitForPageLoad();
    }

    public void VerifyResults(){
        List<WebElement> List = driver.findElements(By.cssSelector("div.Product-contents"));
        System.out.println("Products Shown: " + List.size());
    }
    public void ApplyFilter() throws InterruptedException {
        Filter.click();
        commonActions.waitForPageLoad();
        List<WebElement> List = driver.findElements(By.cssSelector("div.Product-contents"));
        System.out.println("Products Shown After Applying Filter: " + List.size());
    }
    public void VerifyFilterResults(){
        List<WebElement> List = driver.findElements(By.cssSelector("div.Product-contents"));
        int NewList = List.size();
        String FilterText = Filter.getText();
        for(int i=0; i<=NewList; i++){
            FilterOnProductName.getText().contains(FilterText);
            System.out.println("All Products Contains Applied Filter Brand: " + FilterText);
        }

    }
}
